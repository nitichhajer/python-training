import string

n = int(input("Enter: "))
if(n<1 or n>26):
    print ("Invalid")
    exit(0)

letter = dict(zip(range(1, 27), string.ascii_lowercase))

dash = 2*n-2
for i in range(1,n+1):
    
    l = []
    d = ['-'] * dash
          
    for j in (range(n, n-i, -1)):
        l.append(letter[j])
        t = j
    for j in range (t+1, n+1):
        l.append(letter[j])
            
    result = ''.join(d) + '-'.join(l) + ''.join(d)
    print(result)
    
    dash -= 2  

dash= 2
for i in range(n-1, 0, -1):

    l = []
    d = ['-'] * dash
   
    for j in (range(n, n-i, -1)):
        l.append(letter[j])
        t = j
    for j in range (t+1, n+1):
        l.append(letter[j])
               
    result = ''.join(d) + '-'.join(l) + ''.join(d)
    print(result)
    
    dash += 2
    
